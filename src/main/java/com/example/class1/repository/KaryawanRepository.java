package com.example.class1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.KaryawanModel;

public interface KaryawanRepository extends JpaRepository<KaryawanModel, String> {
	
	@Query("SELECT MAX(K.idKaryawan) FROM KaryawanModel K")
	Integer searchMaxId();
	
	@Query("SELECT K FROM KaryawanModel K WHERE K.kodeKaryawan = ?1")
	KaryawanModel searchKodeKaryawan(String kodeKaryawan);
	
	@Query("SELECT K FROM KaryawanModel K WHERE K.isDelete != 1")
	List<KaryawanModel> searchNotDelete();
	
	@Query("SELECT K FROM KaryawanModel K WHERE K.idKaryawan = ?1")//
	KaryawanModel searchIdKaryawan(Integer idKaryawan);
}
