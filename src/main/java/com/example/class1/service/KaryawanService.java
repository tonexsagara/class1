package com.example.class1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.class1.model.KaryawanModel;
import com.example.class1.repository.KaryawanRepository;

@Service
@Transactional
public class KaryawanService {

	@Autowired
	private KaryawanRepository karyawanRepository;
	
	public void create (KaryawanModel karyawanModel) {
		this.karyawanRepository.save(karyawanModel);
	}
	
	public Integer searchMaxId () {
		return this.karyawanRepository.searchMaxId();
	}
	
	public List<KaryawanModel> read() {
		return this.karyawanRepository.findAll();
	}
	
	public void delete(KaryawanModel karyawanmodel) {
		this.karyawanRepository.deleteAll();
	}
	
	public KaryawanModel searchKodeKaryawan(String kodeKaryawan) {
		return this.karyawanRepository.searchKodeKaryawan(kodeKaryawan);
	}
	
	public void update(KaryawanModel karyawanModel) {
		this.karyawanRepository.save(karyawanModel);
	}
	
	public List<KaryawanModel> searchNotDelete() {
		return this.karyawanRepository.searchNotDelete();
	}
	
	public KaryawanModel searchIdKaryawan(Integer idKaryawan) {
		return this.karyawanRepository.searchIdKaryawan(idKaryawan);
	}
}
