package com.example.class1.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_ANGGOTA")
public class AnggotaModel {

	@Id
	@Column(name = "ID_Anggota")
	private Integer idAnggota;
	
	@Column(name = "KD_Anggota", nullable = false, length =25)
	private String kodeAnggota;

	@Column(name = "NM_Anggota", nullable = false, length =25)
	private String namaAnggota;
	
	@Column(name = "C_Usia")
	private Integer usia;
	
	@Column (name="KD_FAKULTAS")
	private String kodeFakultas;
	

	@ManyToOne	
	@JoinColumn(name = "KD_FAKULTAS", nullable=true, updatable=false, insertable=false)
	private FakultasModel fakultasModel;

	public Integer getIdAnggota() {
		return idAnggota;
	}

	public void setIdAnggota(Integer idAnggota) {
		this.idAnggota = idAnggota;
	}

	public String getKodeAnggota() {
		return kodeAnggota;
	}

	public void setKodeAnggota(String kodeAnggota) {
		this.kodeAnggota = kodeAnggota;
	}

	public String getNamaAnggota() {
		return namaAnggota;
	}

	public void setNamaAnggota(String namaAnggota) {
		this.namaAnggota = namaAnggota;
	}

	public Integer getUsia() {
		return usia;
	}

	public void setUsia(Integer usia) {
		this.usia = usia;
	}
	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public FakultasModel getFakultasModel() {
		return fakultasModel;
	}

	public void setFakultasModel(FakultasModel fakultasModel) {
		this.fakultasModel = fakultasModel;
	}

	
}
