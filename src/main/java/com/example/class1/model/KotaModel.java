package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_KOTA")
public class KotaModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID_KOTA")
	private Integer idKota;
	
	@Column(name = "KD_KOTA")
	private String kodeKota;
	
	@Column(name = "NM_KOTA")
	private String namaKota;
	
	public Integer getIdKota() {
		return idKota;
	}
	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}
	public String getKodeKota() {
		return kodeKota;
	}
	public void setKodeKota(String kodeKota) {
		this.kodeKota = kodeKota;
	}
	public String getNamaKota() {
		return namaKota;
	}
	public void setNamaKota(String namaKota) {
		this.namaKota = namaKota;
	}
	
}
