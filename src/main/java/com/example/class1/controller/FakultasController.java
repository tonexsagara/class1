package com.example.class1.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.FakultasModel;
import com.example.class1.service.FakultasService;

import java.util.ArrayList;
import java.util.List;

@Controller //menyambungkan/memanggil data untuk dijadikan link dan menjalankan perintah2
@RequestMapping ("/fakultas") //meminta data dari folder fakultas
public class FakultasController {
	
	@Autowired
	private FakultasService fakultasService; //memanggil FakultasService (asli) dan menamai dengan fakultasService
	
	@RequestMapping("/home")
	public String fakultas()
	{
		return "/fakultas/home";
	}
	@RequestMapping("/add") 
	public String doAdd()
	{
		return "/fakultas/add";
}
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) { //request untuk mengambil data yang kita akan masukan dan membuatnya di database
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		
		FakultasModel fakultasModel = new FakultasModel(); //instance
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		
		this.fakultasService.save(fakultasModel);
		return "/fakultas/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model) // untuk mengirim data dari back ke front end 
	//model dibuat untuk melempar data
	{
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.read(); //read untuk membaca data fakultasService
		model.addAttribute("fakultasModelList",fakultasModelList); //("") dibundle dan dijadikan nama fakultasModelList
		
		return "/fakultas/list"; //menjalankan folder fakultas lalu ke data list.
	}
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model) {// untuk mengirim data dari back ke front end 
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		model.addAttribute("fakultasModel", fakultasModel);
	
		return "/fakultas/detail"; //menjalankan folder fakultas lalu ke data list.
	}
	@RequestMapping("ubah") // untuk pop up
	public String doUbah(HttpServletRequest request, Model model) {
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		model.addAttribute("fakultasModel", fakultasModel);
		
		return "/fakultas/edit";
	}
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) { //request untuk mengambil data yang kita akan masukan dan membuatnya di database
		String kodeFakultas = request.getParameter("kodeFakultas");
		String namaFakultas = request.getParameter("namaFakultas");
		
		FakultasModel fakultasModel = new FakultasModel(); //instance
		fakultasModel.setKodeFakultas(kodeFakultas);
		fakultasModel.setNamaFakultas(namaFakultas);
		
		this.fakultasService.update(fakultasModel);
		return "/fakultas/home";
}
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		String kodeFakultas = request.getParameter("kodeFakultas");
		FakultasModel fakultasModel = new FakultasModel();
		fakultasModel = this.fakultasService.searchKodeFakultas(kodeFakultas);
		
		this.fakultasService.delete(fakultasModel);
		
		return "/fakultas/home";
	}
	@RequestMapping("/search")
	public String doSearchNama(HttpServletRequest request, Model model) {
		String namaFakultas = request.getParameter("namaFakultas"); //text field dari fakultas
		
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); //untuk membuat wadah
		fakultasModelList = this.fakultasService.searchNamaFakultas(namaFakultas); //yang akan di ikat
		model.addAttribute("fakultasModelList", fakultasModelList); // berfungsi untuk membungkus dan yang akan dikirimkan
		
		return "/fakultas/search";
}
	@RequestMapping("/cari/kode")
	public String cariKode(HttpServletRequest request, Model model) { // merequest dari back end ke front end
		String kodeController = request.getParameter("kodeHTML"); //kodeHTML yang telah dibuat dari home.html
		System.out.println(kodeController);
		List<FakultasModel> fakultasModelList= this.fakultasService.cariKodeFakultasDonk(kodeController);
		model.addAttribute("fakultasModelList", fakultasModelList);
		
		return "/fakultas/home";
	}
}