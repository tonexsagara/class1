package com.example.class1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ContohController {

	@RequestMapping("/")
	public String utama() {
		return "/utama";
	}
}
